﻿#include <iostream>
#include <string>

#include "Cargo.h"
#include "Vehicle.h"
using namespace std;

template <class T > 
class NamedObject : T {
private:
	string name;
public:
	NamedObject(string Name, float weight, float volume) : T (weight, volume) {
		this->name = Name;
	};
	void print() override {
		T::print();
		cout << " " << name << endl;
	}
};



int main()
{
	setlocale(LC_ALL, "");

	auto no1 = NamedObject<Cargo>("Селедка", 100, 200);
	auto no2 = NamedObject<Vehicle>("Трубовоз", 3000, 4000);

	no1.print();
	no2.print();
}
