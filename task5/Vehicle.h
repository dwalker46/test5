#pragma once
#include <iostream>

class Vehicle
{
private:
	float volume;
	float weight;
public:
	Vehicle(float weight, float volume) {
		this->volume = volume;
		this->weight = weight;
	}

	virtual ~Vehicle() = default;

	virtual void print() {
		std::cout << "VH: volume-" << volume << ",weight-" << weight;
	}
};

