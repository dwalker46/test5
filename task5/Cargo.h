#pragma once
#include <iostream>

class Cargo
{
private:
		float volume;
		float weight;
public:
		Cargo(float weight, float volume) {
			this->volume = volume;
			this->weight = weight;
		}
		virtual ~Cargo() = default;

		virtual void print() {
			std::cout << "Cargo: volume-" << volume << ",weight-" << weight;
		}
};

